from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index


# Create your tests here.
class lab8TestUnit(TestCase):
    def test_lab8_url_is_exist(self):
        res = Client().get('/lab-8/')
        self.assertEqual(res.status_code, 200)

    def test_lab8_using_index_func(self):
        found = resolve('/lab-8/')
        self.assertEqual(found.func, index)
